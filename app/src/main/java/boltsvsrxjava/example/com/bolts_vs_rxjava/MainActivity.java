package boltsvsrxjava.example.com.bolts_vs_rxjava;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.concurrent.Callable;

import bolts.Continuation;
import bolts.Task;
import boltsvsrxjava.example.com.bolts_vs_rxjava.R;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        runTasks();
    }

    private void runTasks() {
        Task exampleTask = Task.callInBackground(new DummyCallable());
        Task<String> result1 = exampleTask.continueWith(new Continuation<Integer, String>() {
            @Override
            public String then(Task<Integer> task) {
                return "a"; //setup a string that cannot be handled by the next task
            }
        });
        Task<Double> result2 = result1.onSuccess(new Continuation<String, Double>() {
            @Override
            public Double then(Task<String> task) throws Exception {
                //if you throw an exception inside a continuation, the resulting task will be faulted with that exception
                //This task will throw an exception because it cannot convert a to a double
                Double doubleResult = Double.valueOf(task.getResult());
                return doubleResult;
            }
        });
        Task<Boolean> result3 = result2.onSuccess(new Continuation<Double, Boolean>() {
            @Override
            public Boolean then(Task<Double> task) throws Exception {
                Double doubleResult = Double.valueOf(task.getResult());
                return true;
            }
        });
        Task<Void> finalTask = result3.continueWith(new Continuation<Boolean, Void>() {
            @Override
            public Void then(Task<Boolean> task) throws Exception {
                Context applicationContext = getApplication().getApplicationContext();
                if (task.isFaulted()) {
                    Exception exception = task.getError();
                    Toast.makeText(applicationContext, "There was an error "+exception.getMessage(), Toast.LENGTH_LONG).show();

                } else if (task.isCancelled()) {
                    Toast.makeText(applicationContext, "One of the tasks was canceled", Toast.LENGTH_LONG).show();
                } else {
                    String finalResult = String.valueOf(task.getResult());
                    Toast.makeText(applicationContext, "All tasks completed sucessfully" + finalResult, Toast.LENGTH_LONG).show();
                }
                return null;
            }
        }, Task.UI_THREAD_EXECUTOR);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class DummyCallable implements Callable<Integer> {
        @Override
        public Integer call() throws Exception {
            Thread.sleep(100);
            return 5;
        }
    }
}
