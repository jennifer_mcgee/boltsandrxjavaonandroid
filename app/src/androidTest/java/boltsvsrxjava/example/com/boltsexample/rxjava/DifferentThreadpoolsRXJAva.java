package boltsvsrxjava.example.com.boltsexample.rxjava;

import android.test.InstrumentationTestCase;
import android.util.Log;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/*
Example code showing how to set which executor the subscription and observers run on
Note that we can only set the threadpool for the observer and subscriber
It doesn't look like we can set different threads for the map steps
 */
public class DifferentThreadpoolsRXJAva extends InstrumentationTestCase {
    private String TAG = this.getName();

    static final Executor NETWORK_EXECUTOR = Executors.newCachedThreadPool();
    static final Executor DISK_EXECUTOR = Executors.newFixedThreadPool(2);

    public void test(){

        Observable<Integer> myObservable = Observable.just(5);

        Action1<Integer> onNextAction = new Action1<Integer>() {
            @Override
            public void call(Integer s) {
                assertEquals(5, s.intValue());
            }
        };

        myObservable.subscribeOn(Schedulers.from(NETWORK_EXECUTOR));
        myObservable.observeOn(Schedulers.from(DISK_EXECUTOR));
        myObservable.subscribe(onNextAction); //only overwrote the standard action, not on error or onCompleted
    }


}
