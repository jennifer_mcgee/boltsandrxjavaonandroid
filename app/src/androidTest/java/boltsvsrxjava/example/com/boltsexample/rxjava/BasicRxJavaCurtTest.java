package boltsvsrxjava.example.com.boltsexample.rxjava;

import android.test.InstrumentationTestCase;
import android.util.Log;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;


public class BasicRxJavaCurtTest extends InstrumentationTestCase {
    String TAG = BasicRxJavaCurtTest.class.getSimpleName();
    public void testBasicRXJava(){

        Observable<Integer> myObservable = Observable.just(5);

        Action1<Integer> onNextAction = new Action1<Integer>() {
            @Override
            public void call(Integer input) {
                Log.d(TAG, "Result equals "+input);
                assertEquals(5, input.intValue());
            }
        };

        myObservable.subscribe(onNextAction); //only overwrote the standard action, not on error or onCompleted


    }



}
