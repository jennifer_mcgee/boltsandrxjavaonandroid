package boltsvsrxjava.example.com.boltsexample.rxjava;

import android.test.InstrumentationTestCase;
import android.util.Log;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;
import rx.functions.Func1;


public class ComplicatedChainedStepsRxJava extends InstrumentationTestCase {
    private String TAG = this.getName();
    public void testBasicRXJava(){

        Observable<Integer> myObservable = Observable.just(5);

        myObservable.map(new Func1<Integer, String>() { //Added the map operator
            @Override
            public String call(Integer input) {
                return Integer.toString(input);
            }
        }).map( new Func1<String, Double>() {
            @Override
            public Double call(String s) {
                return Double.valueOf(s);
            }
        }).map( new Func1<Double, Boolean>() {
            @Override
            public Boolean call(Double input) {
                if(input == 5){
                    return true;
                }
                else{
                    return false;
                }
            }
        }).subscribe(new Subscriber<Boolean>() {
            @Override
            public void onCompleted() {
                //RXJava has the concept of completed
                Log.d(TAG, "OnComplete called");
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "There was an error " + e.getMessage());
                assertTrue("There was an error", false);
            }

            @Override
            public void onNext(Boolean finalResult) {
                Log.d(TAG, "All tasks completed sucessfully" + finalResult);
                assertEquals(true, finalResult.booleanValue());
            }
        });
    }



}
