package boltsvsrxjava.example.com.boltsexample.rxjava;

import android.test.InstrumentationTestCase;
import android.util.Log;

import java.io.File;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;


public class BasicRxJavaVerboseTest extends InstrumentationTestCase {
    private String TAG = this.getName();
    public void testBasicRXJava(){

        Observable<String> myObservable = Observable.create(
                new Observable.OnSubscribe<String>() {
                    @Override
                    public void call(Subscriber<? super String> sub) {
                        sub.onNext("Hello, world!");
                        sub.onCompleted();
                    }
                }
        );

        Subscriber<String> mySubscriber = new Subscriber<String>() {
            @Override
            public void onNext(String s) {
                Log.d(TAG, s);
                assertEquals("Hello, world!",s); //Make sure we got to the data from the caller
            }

            @Override
            public void onCompleted() { }

            @Override
            public void onError(Throwable e) { }
        };

        myObservable.subscribe(mySubscriber);
    }



}
