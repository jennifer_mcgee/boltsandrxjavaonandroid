package boltsvsrxjava.example.com.boltsexample.rxjava;

import android.test.InstrumentationTestCase;
import android.util.Log;

import bolts.Continuation;
import bolts.Task;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;

/**
 * Good for wrapping thrid party callbacks or other coded that you can't change
 */
public class IntegrateWithCallbackRxJava extends InstrumentationTestCase {

    ClassThatTakesCallback stubApi = new ClassThatTakesCallback();
    Observable<Integer> myObservable;
    private String TAG = this.getName();
    public void test(){

        int input = 2;
        stubApi.methodThatTakesCallback(input, new OnTaskCompleted(){
            @Override
            public void onTaskCompleted(int result) {
                myObservable = Observable.just(result);
            }
        });


        Subscriber<Integer> mySubscriber = new Subscriber<Integer>() {
            @Override
            public void onNext(Integer input) {
                assertEquals(4, input.intValue());
            }

            @Override
            public void onCompleted() { }

            @Override
            public void onError(Throwable e) {
                assertTrue(false); //If in an exception fail
            }
        };

        myObservable.subscribe(mySubscriber); //only overwrote the standard action, not on error or onCompleted
    }


    //Stub thirdparty api that uses callback. Everything under this is taking the place of whatever other api you would use
    private interface OnTaskCompleted{
        void onTaskCompleted(int result);
    }

    private class ClassThatTakesCallback{
        public void methodThatTakesCallback(int input, OnTaskCompleted listener){
            listener.onTaskCompleted(input*2); //do some stub processing and return on the listener

        }
    }


}

