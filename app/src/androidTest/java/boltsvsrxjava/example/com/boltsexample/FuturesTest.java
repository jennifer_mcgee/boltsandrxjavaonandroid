package boltsvsrxjava.example.com.boltsexample;

import android.test.InstrumentationTestCase;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by jennifer.mcgee on 2/2/15.
 */
public class FuturesTest extends InstrumentationTestCase {

    public void testBasicFutures(){
        ExecutorService pool = Executors.newFixedThreadPool(10);
        Future<Integer> firstFuture = pool.submit(new FirstDummyCallable()); //This isn't blocking
        Future<Integer> secondFuture = pool.submit(new SecondDummyCallable()); //This isn't blocking

        try {
            int result1 = firstFuture.get(); //This is blocking
            int result2 = secondFuture.get(); //This is blocking
            pool.shutdown();
            //BUT both execute at the same time and then can be combined
            int endResult = result1 + result2;
            assertEquals(6, endResult);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private class FirstDummyCallable implements Callable<Integer> {
        @Override
        public Integer call() throws Exception {
            Thread.sleep(2000);
            return 5;
        }
    }

    private class SecondDummyCallable implements Callable<Integer> {
        @Override
        public Integer call() throws Exception {
            Thread.sleep(4000);
            return 1;
        }
    }

    //Disadvanteages :
    //1) need a threedpool or something else to construct the future
    //2)can't chain
    //3) no built is flow for error handling
}